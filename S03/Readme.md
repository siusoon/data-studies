# Session 3
Block 1: Data Capture  

**SLIDES**: ([live](http://pablov.me/pres/ds19s03.html) / pdf)

## Plan for the day:
* Presentations
* Data contexts: information
* Text analysis with *Voyant*

## Data Contexts: information

<img src="DIKW.png" alt="drawing" width="500">

*Knowledge pyramid (Kitchin, adapted from Adler 1986 and McCandless 2010)*

### Cybernetics 
* **Shannon** (1948)

  > “Information is a probability, a function, with no dimensions, no materiality, and no necessary connection with meaning”

   * information is about signals, not meaning
   * information must be finite

* **Weiner** (1961)

  > “Information is information, not matter or energy”

   * information as organised communication
   * (the more probable the message, the less information it gives)

* **Capurro and Hjørland** (2003)

   * information as the act of communicating meaning



> *A line is a dot that went for a walk* (Paul Klee)

<img src="new_harmony.jpg" alt="drawing" width="500">

*New Harmony*

<img src="PlaceFinal.png" alt="drawing" width="500">

*Place* (**Click the image for a bigger version. You can also find the gif [here](place.gif)**)


## Short activity: 

1. Identify “things” that you recognise in Place. 
2. Note down a research question for Place (i.e. what would you like to *know* about it?)

### Data contexts (POST)
* STS/ANT
* Media theory (Kittler 1999 [1986])
* Cultural analytics
* Digital humanities
* Software studies
* Digital Methods / Issue mapping


## Tools
### Voyant Tools
https://voyant-tools.org/

* web-based tool (running on voyant servers, but can be installed locally)
* Corpus analysis is a type of content analysis that allows large-scale comparisons to be made on a set of texts or corpus.
* processed *in the cloud* (although it can be installed on a local server), so be careful with your inputs (e.g. do not use sensitive data with this tool)
* Clinton-Trump corpus zip is [here](Clinton-Trump-Corpus.zip)

After uploading your file in the main page, you will reach the 'interface' ('skin') that has five tools by default. Here is a brief explanation of each of these tools:

* **Cirrus**: word cloud that shows the most frequent terms
* **Reader**: space for the revision and reading of the complete texts with a bar graph that indicates the amount of text of each document
* **Trends**: distribution chart that shows the terms throughout the corpus (or within a document when only one is loaded)
* **Summary**: provides an overview of certain textual statistics of the current corpus

* **Contexts**: concordance that shows each occurrence of a keyword with some surrounding context

**Things to consider**:

* **Vocabulary density**: number of unique words divided by the total number of words. The closer to one, the greater variety of words (denser).

* **Words per sentence**: The way in which Voyant calculates the length of sentences should be considered very approximate, because It is complciated to distinguish between the end of an abbreviation and that of a sentence or other uses of punctuation. The sentence analysis is performed by a template with instructions or 'class' of the Java programming language called [BreakIterator](https://docs.oracle.com/javase/tutorial/i18n/text/about.html). 

**Editing stop words:**

1. We place our right cursor in the Cirrus window and click on the icon that looks like a switch.
2. A window with different options will appear, select the first “Edit list” (it is recommended to save your empty words list to a separate .txt file, for future reference)


### NLP: Stanford's NER (***this is NOT an obligatory item of the curriculum***)

http://nlp.stanford.edu:8080/ner/ 

Stanford Name Entity Recognition is a Natural Language Processing tool. Among other things, it will help you to quantitatively recognize entities (up to 7 classes), and export your findings for further analysis. NER has 6 language models: English (very complete), German, Spanish, Arabic, French, and Chinese. The tool is free, Open Sourced, and does not collect any of your data. However, one of the downsides of this tool is that the there is no documentation on how the models were/are constructed.

1. Go to the online teaser version of NLP (http://nlp.stanford.edu:8080/ner/)

2. Paste a short text press “submit” (for the example I'll use [this file](Guardian_Johnson_16-sep-19.txt)). You’ll see some organizations and locations highlighted. Select a different piece of text, and play with the different classifiers. On the online version you’ll have the following options for English:

* 3 class:Location, Person, Organization
* 4 class: Location, Person, Organization, Misc
* 7 class: Location, Person, Organization, Money, Percent, Date, Time

And for Chinese:

* 7 class: Location, Person, Organization, Facility, Demonym, Misc, GPE [geopolitcal entity]

Consider the following:

* How accurate the entity recognition technique seems to be?

* What are the shortcomings and advantages of using NLP tool/technique? (e.g. figures of authority already defined?)

**Recommended for advanced users: (*these are also NOT requirements for the course*)**

* You can download an install your own version, to parse longer texts (however, you’ll need a working installation of Java), and some patience: https://nlp.stanford.edu/software/CRF-NER.html#Download
* Also, the NLTK (Natural Langauge ToolKit) Python library has plenty of resources to work with text (including Name Entity Recognition): http://www.nltk.org/ 

## Activities

1. Calculate the density of 2 songs of very different style in Voyant (*½* *wikipoint*)
   
2. Choose one of the following:

   * Compare the speeches of Trump and Clinton in Voyant. What are the main differences? What does voyant can tell you about the use of words? For example, which are the most frequent words? Do some terms become more relevant at an specific time? Is it possible to get an idea of the most important topics (not words), in each type of discourse? (*9000 wikipoints*)

   * If you scraped text during your last mini project, see if you can generate some insights from it using Voyant. Otherwise, explore get a one or more texts from [Project Gutenberg](https://www.gutenberg.org/catalog/) (remember to download the plain text version). What does voyant allows you to see, that was not obvious before? Is your data format revealing? Which data formats are not useful for a tool like Voyant? (*5000 wikipoints*)

3. (***Optional***): Paste a small text of your choice in Stanford’s NER. How accurate the entity recognition technique seems to be? What are the shortcomings and advantages of using NLP tool/technique? (*5^5 wikipoints*)

## Required Readings
* Beer, David. The Data Gaze: Capitalism, Power and Perception. 1 edition. Thousand Oaks, CA: SAGE Publications Ltd, 2018. (Chapter 5: the diagnostic eye, 30 p.)
* Ruppert, Evelyn. “Category.” In Inventive Methods: The Happening of the Social, edited by Celia Lury and Nina Wakeford, 36–47. Routledge, 2012. (11 p.)

## Recommended readings
### CONTEXT
* Adler, M. J. A Guidebook to Learning: For a Lifelong Pursuit of Wisdom. New York: Macmillan, 1996.
* Berry, David. 2014. ‘Post-Digital Humanities: Computation and Cultural Critique in the Arts and Humanities (EDUCAUSE Review) | EDUCAUSE.Edu’. 19 May 2014. https://www.educause.edu/ero/article/post-digital-humanities-computation-and-cultural-critique-arts-and-humanities 
* Kittler, Friedrich A. 1999. Gramophone, Film, Typewriter. Translated by Geoffrey Winthrop-Young and Michael Wutz. 1 edition. Stanford, Calif: Stanford University Press.
* Manovich, Lev. 2001. The Language of New Media. MIT Press.
* McCandless, David. “Data, Information, Knowledge, Wisdom?” Information is Beautiful. Accessed September 18, 2019. https://informationisbeautiful.net/2010/data-information-knowledge-wisdom/.
* Rogers, Richard. 2013. Digital Methods. MIT Press.
* Shannon, C. E. “A Mathematical Theory of Communication.” Bell System Technical Journal 27, no. 3 (1948): 379–423.
* Wiener, Norbert. Cybernetics: Or, Control and Communication in the Animaland the Machine. Cambridge, Mass: M.I.T. Press, 1961.
### TEXT ANALYSIS
* DiMaggio, Paul. 2015. “Adapting Computational Text Analysis to Social Science (and Vice Versa).” Big Data & Society 2 (2)
* Lin, Yuwei. 2012. “Trans-Disciplinarity and Digital Humanity: Lessons Learned from Developing Text Mining Tools for Textual Analysis.” Textual Analysis’, 18.


