## Recommended bibliography per topic
* *The ones in **bold** were required readings for the course*
* *You are not limited to this bibliography!*

#### ETHICS
* **Rogers, Richard. “Foundations of Digital Methods.” In The Datafied Society. Studying Culture through Data, 75–94. Amsterdam University Press, 2017. (20 p.)** 
* **Annette N. Markham. “Research Ethics in Context.” In The Datafied Society. Studying Culture through Data, edited by van Es Karin and Schäfer Mirko Tobias, 201–9. Amsterdam University Press, 2017. (19 p.)**
* Hewson, Claire. “Ethics Issues in Digital Methods Research.” In Digital Methods for Social Science: An Interdisciplinary Guide to Research Innovation, edited by Helene Snee, Christine Hine, Yvette Morey, Steven Roberts, and Hayley Watson, 2016. http://www.dawsonera.com/depp/reader/protected/external/AbstractView/S9781137453662. (16 p.)
* Markham, Annette. “Ethic as Method, Method as Ethic: A Case for Reflexivity in Qualitative ICT Research.” Journal of Information Ethics 15, no. 2 (November 1, 2006): 37–54. https://doi.org/10.3172/JIE.15.2.37. (17 p.)
Kaiser, Jonas, and Adrian Rauchfleisch. “The Implications of Venturing down the Rabbit Hole.” Internet Policy Review, June 27, 2019. https://policyreview.info/articles/news/implications-venturing-down-rabbit-hole/1406. (8 p.)
* Kotsios, Andreas, Matteo Magnani, Luca Rossi, Irina Shklovski, and Davide Vega. “An Analysis of the Consequences of the General Data Protection Regulation (GDPR) on Social Network Research.” ArXiv:1903.03196 [Cs], March 7, 2019. http://arxiv.org/abs/1903.03196.

#### "DATA"
* **Daly, Angela, Kate Devitt, and Monique Mann, eds. Good Data. Theory on Demand 29. Amsterdam: Institute of Network Cultures, 2019. (Chapter 10: Making Data Public? The Open Data Index as Participatory Device, 15 p.)**
* **Gitelman, Lisa. Raw Data Is an Oxymoron. MIT Press, 2013. (chapter 1: Data before the fact, 25 p.)**
* Becker, Howard. Through Values to Social Interpretation; Essays on Social Contexts, Actions, Types, and Prospects. Through Values to Social Interpretation; Essays on Social Contexts, Actions, Types, and Prospects. Oxford, England: Duke Univ. Press, 1950.
* Drucker, Johanna. “Humanities Approaches to Graphical Display.” Digital Humanities Quarterly 005, no. 1 (March 10, 2011).
* Floridi, Luciano. Information: A Very Short Introduction. OUP Oxford, 2010. https://www.statsbiblioteket.dk/au/?locale=en#/search?query=recordID:"ebog_ssj0000523414"
* Kitchin, Rob, Nick Tate, and Nick Tate. Conducting Research in Human Geography : Theory, Methodology and Practice. Routledge, 2013.
* Kitchin, Rob. The Data Revolution: Big Data, Open Data, Data Infrastructures and Their Consequences. SAGE, 2014.
* Schöch, Christof. “Big? Smart? Clean? Messy? Data in the Humanities.” Journal of Digital Humanities, November 22, 2013. http://journalofdigitalhumanities.org/2-3/big-smart-clean-messy-data-in-the-humanities/.
* Wilkinson, Mark D., Michel Dumontier, IJsbrand Jan Aalbersberg, Gabrielle Appleton, Myles Axton, Arie Baak, Niklas Blomberg, et al. “The FAIR Guiding Principles for Scientific Data Management and Stewardship.” Scientific Data 3, no. 1 (December 2016): 160018. https://doi.org/10.1038/sdata.2016.18.

#### CONTEXT AND DIGITAL METHODS
* Adler, M. J. A Guidebook to Learning: For a Lifelong Pursuit of Wisdom. New York: Macmillan, 1996.
* Berry, David. 2014. ‘Post-Digital Humanities: Computation and Cultural Critique in the Arts and Humanities (EDUCAUSE Review) | EDUCAUSE.Edu’. 19 May 2014. https://www.educause.edu/ero/article/post-digital-humanities-computation-and-cultural-critique-arts-and-humanities 
* Kittler, Friedrich A. 1999. Gramophone, Film, Typewriter. Translated by Geoffrey Winthrop-Young and Michael Wutz. 1 edition. Stanford, Calif: Stanford University Press.
* Manovich, Lev. 2001. The Language of New Media. MIT Press.
* McCandless, David. “Data, Information, Knowledge, Wisdom?” Information is Beautiful. Accessed September 18, 2019. https://informationisbeautiful.net/2010/data-information-knowledge-wisdom/.
* Shannon, C. E. “A Mathematical Theory of Communication.” Bell System Technical Journal 27, no. 3 (1948): 379–423.
* Wiener, Norbert. Cybernetics: Or, Control and Communication in the Animal and the Machine. Cambridge, Mass: M.I.T. Press, 1961.

#### TEXT ANALYSIS
* DiMaggio, Paul. 2015. “Adapting Computational Text Analysis to Social Science (and Vice Versa).” Big Data & Society 2 (2)
* Lin, Yuwei. 2012. “Trans-Disciplinarity and Digital Humanity: Lessons Learned from Developing Text Mining Tools for Textual Analysis.” Textual Analysis’, 18.

#### PLATFORMS
* **Beer, David. The Data Gaze: Capitalism, Power and Perception. 1 edition. Thousand Oaks, CA: SAGE Publications Ltd, 2018. (Chapter 5: the diagnostic eye, 30 p.)**
* **Ruppert, Evelyn. “Category.” In Inventive Methods: The Happening of the Social, edited by Celia Lury and Nina Wakeford, 36–47. Routledge, 2012. (11 p.)**
* Bogost, Ian, and Nick Montfort. “Platform Studies: Frequently Questioned Answers,” December 12, 2009. https://escholarship.org/uc/item/01r0k9br.
* Gawer, Annabelle. “Bridging Differing Perspectives on Technological Platforms: Toward an Integrative Framework.” Research Policy 43, no. 7 (September 1, 2014): 1239–49. https://doi.org/10.1016/j.respol.2014.03.006.
* Gillespie, Tarleton. “The Politics of ‘Platforms.’” New Media & Society 12, no. 3 (May 2010): 347–64. https://doi.org/10.1177/1461444809342738.
* Helmond, Anne, David B. Nieborg, and Fernando N. van der Vlist. “Facebook’s Evolution: Development of a Platform-as-Infrastructure.” Internet Histories 3, no. 2 (April 3, 2019): 123–46. https://doi.org/10.1080/24701475.2019.1593667.
* Helmond, Anne. “The Platformization of the Web: Making Web Data Platform Ready.” Social Media + Society 1, no. 2 (July 1, 2015): 2056305115603080. https://doi.org/10.1177/2056305115603080.
* John, Nicholas A., and Asaf Nissenbaum. “An Agnotological Analysis of APIs: Or, Disconnectivity and the Ideological Limits of Our Knowledge of Social Media.” The Information Society 35, no. 1 (January 2019): 1–12. 
* Rieder, Bernhard, and Guillaume Sire. “Conflicts of Interest and Incentives to Bias: A Microeconomic Critique of Google’s Tangled Position on the Web.” New Media & Society 16, no. 2 (March 1, 2014): 195–211. https://doi.org/10.1177/1461444813481195.
* Zomorodi, Manoush. “Facing Our Weirdest Selves 2016.” Note to Self. Accessed September 26, 2018. https://www.wnycstudios.org/story/dear-data-quantified-self-tracking. (PODCAST)

#### INSTAGRAM
* Hochman, Nadav. “Visualizing Instagram: Tracing Cultural Visual Rhythms.” In International AAAI Conference on Web and Social Media, 4, 2012. https://www.aaai.org/ocs/index.php/ICWSM/ICWSM12/paper/view/4782/5091. (4 p.)
* Hochman, Nadav, and Lev Manovich. “Zooming into an Instagram City: Reading the Local through Social Media.” First Monday 18, no. 7 (June 17, 2013). http://firstmonday.org/ojs/index.php/fm/article/view/4711. (~50 p.)
* Hu, Yuheng, Lydia Manikonda, and Subbarao Kambhampati. “What We Instagram: A First Analysis of Instagram Photo Content and User Types,” n.d., 4. (4p.)

#### VISUALIZATION 
* Posavec, Stefanie, Giorgia Lupi, and Maria Popova. Dear Data. New York: Princeton Architectural Press, 2016.
* Data is beautiful https://www.reddit.com/r/dataisbeautiful/
* Mona chalabi infographics https://www.instagram.com/monachalabi/
* **Malik, Momin M. “Identifying Platform Effects in Social Media Data,” n.d., 9. (9 p.) [ignore equations!]**
* **Lury, Celia, Rachel Fensham, Alexandra Heller-Nicholas, Sybille Lammes, Angela Last, Mike Michael, and Emma Uprichard, eds. Routledge Handbook of Interdisciplinary Research Methods. 1st ed. Routledge, 2018. (Section 2, Chapter 12: visualizing data, 10 p.)**
* Drucker, Johanna. Graphesis: Visual Forms of Knowledge Production. Cambridge, Massachusetts: Harvard University Press, 2014.
* Halpern, Orit. Beautiful Data: A History of Vision and Reason since 1945. Durham: Duke University Press Books, 2015.
* Peck, Evan M., Sofia E. Ayuso, and Omar El-Etr. “Data Is Personal: Attitudes and Perceptions of Data Visualization in Rural Pennsylvania.” ArXiv:1901.01920 [Cs], January 7, 2019. http://arxiv.org/abs/1901.01920.

#### MAPS/TERRITORY
* Dodge, Martin, Rob Kitchin, and Chris Perkins. 2011. ‘Conceptualising Mapping’. In The Map Reader: Theories of Mapping Practice and Cartographic Representation, 1–7. Chichester, UK: John Wiley & Sons, Ltd.
* Eichenser, Kristen. “Data Extraterritoriality.” Texas Law Review, November 10, 2017. https://texaslawreview.org/data-extraterritoriality/.
* Bratton, Benjamin H. 2016. The Stack: On Software and Sovereignty. 1 edition. Cambridge, Massachusetts: The MIT Press.

#### DIGITAL METHODS
* **Ruppert, Evelyn, John Law, and Mike Savage. “Reassembling Social Science Methods: The Challenge of Digital Devices.” Theory, Culture & Society 30, no. 4 (2013): 22–46. (24 p.)**
* **Marres, Noortje. “The Redistribution of Methods: On Intervention in Digital Social Research, Broadly Conceived.” The Sociological Review 60 (2012): 139–65. (26 p.)**
* Bolter, Jay David, and Richard Grusin. Remediation: Understanding New Media. Reprint edition. Cambridge, Mass.: The MIT Press, 2000.
* Dillman, Don A. Mail and Internet Surveys: The Tailored Design Method -- 2007 Update with New Internet, Visual, and Mixed-Mode Guide. John Wiley & Sons, 2011.
* Fielding, Nigel G. “The Internet as Research Medium.” In The SAGE Handbook of Online Research Methods, by Nigel G. Fielding, Raymond M. Lee, and Grant Blank. SAGE, 2008.
* Herring, Susan C. “Computer-Mediated Discourse Analysis.” In Designing for Virtual Communities in the Service of Learning, edited by Sasha Barab, Rob Kling, and James H. Gray, 338–76. Cambridge: Cambridge University Press, 2004. 
* Hine, Christine. Virtual Ethnography. First edition. London ; Thousand Oaks, Calif: SAGE Publications Ltd, 2000.
* Kazmer, Michelle M., and Bo Xie. “Qualitative Interviewing in Internet Studies: Playing with the Media, Playing with the Method.” Information, Communication & Society 11, no. 2 (March 2008): 257–78.
* Page, Lawrence, Sergey Brin, Rajeev Motwani, and Terry Winograd. “The PageRank Citation Ranking: Bringing Order to the Web.” Techreport, November 11, 1999. http://ilpubs.stanford.edu:8090/422/.
* Rogers, Richard. The End of the Virtual: Digital Methods. Amsterdam University Press, 2009.
* Rogers, Richard. Digital Methods. MIT Press, 2013.
* Rogers, Richard. “Digital Methods for Web Research.” In Emerging Trends in the Social and Behavioral Sciences, edited by Robert A Scott and Stephan M Kosslyn, 1–22. Hoboken, NJ, USA: John Wiley & Sons, Inc., 2015.
* Savage, Mike, and Roger Burrows. “The Coming Crisis of Empirical Sociology.” Sociology 41, no. 5 (October 2007): 885–99.
* Snee, Helene, Christine Hine, Yvette Morey, Steven Roberts, and Hayley Watson. Digital Methods for Social Science: An Interdisciplinary Guide to Research Innovation, 2016. http://www.dawsonera.com/depp/reader/protected/external/AbstractView/S9781137453662.

#### NETWORKS
* **Venturini, Tommaso, Liliana Bounegru, Mathieu Jacomy, and Jonathan Gray. 2017. ‘How to tell stories with networks’. In The Datafied Society. Studying Culture through Data, 155–69. Amsterdam University Press. (14 p.)**
* **Bruns, Axel, and Jean Burgess. “The Use of Twitter Hashtags in the Formation of Ad Hoc Publics,” 2011, 9. (9 p.)**
* **Page, L., Brin, S., Motwani, R., & Winograd, T. (1999). The PageRank citation ranking: Bringing order to the web. Stanford InfoLab. (17 p.) [ignore equations!]**
* Baran, P. 1964. ‘On Distributed Communications Networks’. IEEE Transactions on Communications Systems 12 (1): 1–9.
* Gießmann, Sebastian. 2017. ‘Drawing the Social: Jacob Levy Moreno, Sociometry, and the Rise of Network Diagrammatics’. Collaborative Research Center 1187 Media of Cooperation, Working Paper Series, , December. 
* Guggenheim, Michael. “Laboratizing and De-Laboratizing the World: Changing Sociological Concepts for Places of Knowledge Production.” History of the Human Sciences 25, no. 1 (February 2012): 99–118.
* Heidler, R., Gamper, M., Herz, A., & Eßer, F. (2014). Relationship patterns in the 19th century: The friendship network in a German boys’ school class from 1880 to 1881 revisited. Social Networks, 37, 1–13.
* Latour, Bruno. 2007. Reassembling the Social: An Introduction to Actor-Network-Theory. OUP Oxford.
* **Venturini, Tommaso, Mathieu Jacomy, and Pereira D Carvalho. 2015. ‘Visual Network Analysis’. Sciences Pomedialab.**
* Mayer, Katja. “Objectifying Social Structures: Network Visualization as Means of Social Optimization.” Theory & Psychology 22, no. 2 (April 2012): 162–78. 
* Sánchez, Sandra Álvaro. “A Topological Space For Design, Participation And Production. Tracking Spaces Of Transformation,” no. 13 (n.d.): 15.
* **TCAT:**Borra, Erik, and Bernhard Rieder. “Programmed Method: Developing a Toolset for Capturing and Analyzing Tweets.” Edited by Dr Axel Bruns and Dr Katrin Weller. Aslib Journal of Information Management 66, no. 3 (May 19, 2014): 262–78. 
* **Latour, Bruno, Pablo Jensen, Tommaso Venturini, Sébastian Grauwin, and Dominique Boullier. 2012. ‘“The Whole Is Always Smaller than Its Parts”–a Digital Test of Gabriel Tardes’ Monads’. The British Journal of Sociology 63 (4): 590–615. (25 p.)**
* **Marres, Noortje. 2015. ‘Why Map Issues? On Controversy Analysis as a Digital Method’. Science, Technology & Human Values, 0162243915574602. (24 p.)**
* Bucher, Taina, and Anne Helmond. “The Affordances of Social Media Platforms.” In The SAGE Handbook of Social Media, by Jean Burgess, Alice Marwick, and Thomas Poell, 233–53. 1 Oliver’s Yard, 55 City Road London EC1Y 1SP: SAGE Publications Ltd, 2018.
* Gibson, Rachel, Marta Cantijoch, and Stephen Ward. Analysing Social Media Data and Web Networks, 2014.
* Latour, Bruno. We Have Never Been Modern. Cambridge, Mass: Harvard University Press, 1993.
* Latour, Bruno. Reassembling the Social: An Introduction to Actor-Network-Theory. OUP Oxford, 2007.
* Lury, Celia, and Nina Wakeford. “Introduction: A Perpetual Inventory.” In Inventive Methods: The Happening of the Social, 1–25. Routledge, 2012.
* Marres, Noortje. “Net-Work Is Format Work: Issue Networks and the Sites of Civil Society Politics.” In Reformatting Politics, 33–48. Routledge, 2013.
* Marres, Noortje, and Richard Rogers. “Recipe for Tracing the Fate of Issues and Their Publics on the Web,” 2005.
* Marres, Noortje, and Esther Weltevrede. 2013. ‘Scraping the Social? Issues in Live Social Research’. Journal of Cultural Economy 6 (3): 313–335. (21 p.)
* Mayer, Katja. “Objectifying Social Structures: Network Visualization as Means of Social Optimization.” Theory & Psychology 22, no. 2 (April 2012): 162–78. 
* Rogers, Richard. Doing Digital Methods. Thousand Oaks, CA: SAGE Publications Ltd, 2019.

#### BIG DATA
* **Uprichard, Emma. “Focus: Big Data, Little Questions?” Discover Society, October 1, 2013. (6 p.)**  
* **boyd, danah, and Kate Crawford. “Critical Questions For Big Data: Provocations for a Cultural, Technological, and Scholarly Phenomenon.” Information, Communication & Society 15, no. 5 (June 2012): 662–79. (18 p.)**
* Politics of Big Data. Digital Culture & Society, Volume 2, Issue 2, Pages 1–2, ISSN (Online) 2364-2122, ISSN (Print) 2364-2114 
* Andrejevic, Mark. “‘Framelessness,’ OR THE Cultural Logic OF Big Data,” n.d., 16.
* Karin, van Es, and Schäfer Mirko Tobias. The Datafied Society. Studying Culture through Data. Amsterdam University Press, 2017. Chapter 16: The myth of big data)  

#### SURVEILLANCE
* **Dijck, Jose van. 2014. ‘Datafication, Dataism and Dataveillance: Big Data between Scientific Paradigm and Ideology’. Surveillance & Society 12 (2): 197–208. (11 p.)** 
* **Cathy, O’Neil. Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy. 1 edition. London: Penguin, 2017. (Chapter 10, 18 p.)**
* David Lyon, and Bauman, Zygmunt. Liquid Surveillance: A Conversation. 1 edition. Cambridge, UK ; Malden, MA: Polity, 2012.
* Elmer, Greg. “A Diagram of Panoptic Surveillance.” New Media & Society 5, no. 2 (June 1, 2003): 231–47.
* Foucault, Michel. Discipline and Punish: The Birth of the Prison. Vintage. Knopf Doubleday Publishing Group, 2012 [1975].
* Gitelman, Lisa. Raw Data Is an Oxymoron. MIT Press, 2013. (Chapter 7: Dataveillance and Counterveillance, 24 p.)
* Landa, Manuel De. War in the Age of Intelligent Machines. New York, NY: Zone Books, 1991.
* Lupton, Deborah. The Quantified Self. John Wiley & Sons, 2014.
* Mann, S. (2016). Surveillance (Oversight), Sousveillance (Undersight), and Metaveillance (Seeing Sight Itself). 2016 IEEE Conference on Computer Vision and Pattern Recognition Workshops (CVPRW), 1408–1417.
* Marwick, Alice E. “Social Surveillance in Everyday Life.” Suveillance & Society 9, no. 4 (2012): 378–93.
* Srnicek, Nick. 2016. Platform Capitalism. 1 edition. Cambridge, UK ; Malden, MA: Polity.

#### DATA LOGIC
* **Agre, Philip E. 1994. ‘Surveillance and Capture: Two Models of Privacy’. The Information Society 10 (2): 101–27.**
* **Pasquinelli, Matteo, ed. Alleys of Your Mind. Lüneburg: meson press, 2015. (Chapter 1: Texeira, Ana, The pigeon in the machine, 14 p.)**
* **Hear (podcast) Reply All, The Crime Machine ([part I](https://gimletmedia.com/shows/reply-all/o2hx34/) and [part II](https://gimletmedia.com/shows/reply-all/n8hwl7))**
* Anderson, Chris. “The End of Theory: The Data Deluge Makes the Scientific Method Obsolete.” Wired, June 23, 2008. https://www.wired.com/2008/06/pb-theory/.
* Beniger, James R. The Control Revolution: Technological and Economic Origins of the Information Society. Cambridge, Mass. ; London: Harvard University Press, 1986.
* Bucher, Taina. If...Then: Algorithmic Power and Politics. Oxford Studies in Digital Politics. Oxford, New York: Oxford University Press, 2018. (Introduction, 18p.)
* Engels, Friedrich. “On Authority.” In Marx-Engels Reader, 2nd ed., 730–33. New York: W. W. Norton and Co, 1978.
* Galloway, Alexander R. Protocol: How Control Exists After Decentralization. MIT Press, 2004.
* Gerlitz, Carolin, and Anne Helmond. “The like Economy: Social Buttons and the Data-Intensive Web.” New Media & Society 15, no. 8 (December 1, 2013): 1348–65. (17 p.)
* Kittler, Friedrich A. “Code (or, How You Can Write Something Differently).” In Software Studies: A Lexicon, by Matthew Fuller. MIT Press, 2008.
* Medina, Eden. Cybernetic Revolutionaries: Technology and Politics in Allende’s Chile. MIT Press, 2014.
* Rouvroy, Antoinette. “The End(s) of Critique : Data-Behaviourism vs. Due-Process.” In Privacy, Due Process and the Computational Turn, edited by Mireille Hildebrandt, 1 edition. Abingdon; New York: Routledge, 2015.
* Searle, John R. “Minds, Brains, and Programs.” Behavioral and Brain Sciences 3, no. 3 (September 1980): 417–24.
* Wiener, Norbert. 1965. Cybernetics: Or, Control and Communication in the Animaland the Machine. 2d pbk. ed. The M.I.T. Paperback Series. Cambridge, Mass: M.I.T. Press.
* Winner, Langdon. 1980. ‘Do Artifacts Have Politics?’ Daedalus 109 (1): 121–136.

#### RESISTANCE TACTICS

**I strongly recommend to take a look at the INC publications catalogue** (most of them are free): https://networkcultures.org/publications/  
* **Colakides, Yiannis. State Machines: Reflections and Actions at the Edge of Digital Citizenship, Finance, and Art. Amsterdam: Institute of Network Cultures, 2019. (Chapter: “There is no anonymity in the database”, 12 p.)**
* **Daly, Angela, Kate Devitt, and Monique Mann, eds. Good Data. Theory on Demand 29. Amsterdam: Institute of Network Cultures, 2019. (Chapter 14: Data for the Social Good: Toward a Data-Activist Research Agenda, 16 p.)** 
* Reply All #112 The Prophet. Gimlet Media,  Accessed November 25, 2019. https://gimletmedia.com/shows/reply-all/j4hl36.
* This Is Not an Atlas: A Global Collection of Counter-Cartographies. First edition. Sozial- Und Kulturgeographie, volume 26. Bielefeld: Transcript, 2018.
* Goldsmith, Jack, and Tim Wu. Who Controls the Internet?: Illusions of a Borderless World. New York: Oxford University Press, 2008.
* Gutiérrez, Miren, and Stefania Milan. “Playing with Data and Its Consequences.” First Monday 24, no. 1 (January 3, 2019).
* Hands, Joss. @ Is for Activism: Dissent, Resitance and Rebellion in a Digital Culture. London ; New York, NY: Pluto, 2011.
* Lessig, Lawrence. Code: And Other Laws of Cyberspace, Version 2.0. 2nd Revised ed. edition. New York: Basic Books, 2006.
* Scholz, Trebor. “Platform Cooperativism vs. the Sharing Economy.” Medium, July 10, 2015. https://medium.com/@trebors/platform-cooperativism-vs-the-sharing-economy-2ea737f1b5ad.
* Scholz, Trebor, and Nathan Schneider. Ours to Hack and to Own. New York, 2017.
* Stallman, Richard, Joshua Gay, and Free Software Foundation (Cambridge Mass.). Free Software, Free Society: Selected Essays of Richard M. Stallman. Free Software Foundation, 2002.

## Other Resources
* Periodic table of visualisation methods: http://www.visual-literacy.org/periodic_table/periodic_table.html   *This table is really useful!**
* Documentary: The Internet’s Own Boy: The Story of Aaron Swartz. Accessed November 20, 2019. http://www.imdb.com/title/tt3268458/.
* Podcast: [Data is Personal with Evan Peck](https://datastori.es/data-is-personal-with-evan-peck/) (Data Stories #142)
* Podcast: Reply All "The crime machine I" Accessed July 13, 2019. https://gimletmedia.com/shows/reply-all/o2hx34/.
* Podcast: Reply All "The crime machine I" Accessed July 13, 2019. https://gimletmedia.com/shows/reply-all/n8hwl7.
* Podcast: Radiolab. “Eye in the Sky | Radiolab.” Accessed July 13, 2019. https://www.wnycstudios.org/podcasts/radiolab/articles/eye-sky.
* Podcast: Note to Self. “When Your Conspiracy Theory Is True.” Accessed November 13, 2019. https://www.wnycstudios.org/podcasts/notetoself/episodes/stingray-conspiracy-theory-daniel-rigmaiden-radiolab.
* Website project: Moll, Joanna and Tactical Tech, The Dating Brokers. https://datadating.tacticaltech.org/viz. Accessed August 15, 2019. 
