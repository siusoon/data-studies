# Session 5
Block 2: Platforms, Maps & Beautiful Data 

**SLIDES**: ([live](http://pablov.me/pres/ds19s05.html) / pdf)
## Downloads & installations prior to session:
- **Sign up for a Carto student account using Github student pack:**
https://carto.com/help/getting-started/student-accounts/#github-student-dev-pack---the-process  
(you can also sign up for a free [temporary non-student account](https://carto.com/signup/), if you have problems with the github option)

## Plan for the day:
* presentations
* Data contexts: visualisation (II)
* Tool: Carto

## Data contexts: visualisation (II)

### Data is personal (Peck et al. 2019)

<img src="peck_graphs1.png" width="500">
<img src="peck_graphs2.png" width="500">

Link to the Podcast Episode about the study: https://player.fm/series/data-stories/ep-142-data-is-personal-with-evan-peck

### Graphesis (Drucker 2014)
* "information graphics": metrics expressed as graphics
* "visual epistemology":  ways of knowing, visually
* Kandinsky -> Gestalt -> Information display and analysis
* Playfair: rationality (cartesian quadrants)
* Ways to visualise *interpretation* are needed (graphics for humanism)
> The bivariate graph, with its inexhaustible capacity to spatialize parameters and put them into relation with each other, is an intellectual product of an era in which rationality could be ut at the service of theoretical and practical knowledge

<img src="buno400.jpg" width="500"><br>  
Johannes Buno the 400 (year 400s)  

<img src="playfair.jpg" width="500"><br>   
William Playfair, Scotland's imports and exports 1780-1781(1786)  

<img src="minard.png" width="500"><br>
Charles Jospeh Minard, Napoleon's army across Russian Empire of Alexander I (1869)  

<img src="gestalt.jpg" width="500"><br>  

### Beautiful Data (Halpern 2014)
* Cybernetics (kubernetes)
 * end of natural exploration 
 * instead: organisation of complex systems
 * based on management & analysis of data
 * beautiful =  elegant, useful -> able to produce value
 * consumers not as 
 > individual subjects, but rather as recombined units of attention, behaviour, and credit

### Maps / Geo
* A particular kind of visual data
* A representation of power

<img src="hereford.jpg" width="500"><br>  
* Hereford map (c. 1300) - Jersualem at the centre of the circle

<img src="borgia.jpg" width="500"><br>     
Borgia map (c. 1450) -  Babilonian, Alexandrian, Carthagian, and Roman empire (extremes of the world as monstruos races)

<img src="regina.jpg" width="500"><br>    
Europa Regina map (c. 1500) - Spain as the crown (Habsburg empire) 

* Critical cartography: modern maps not as an evolution

**Data about places (maps)**
* Territory / census
* Extended geographical information: e.g. Google streetview in Germany 

**Data between places (flows)**
* Extraterritoriality of data (Eichenser)
* Bitcoin example

* Reformulation of concepts like "space, "territory", "population"

### Short activity: 
Visit:  
**a.** Maps that “explain” the internet https://www.vox.com/a/internet-maps and select 1 or 2 examples

AND

**b.** Traceroute https://traceroute-online.com and trace a route from any domain

And answer one or more of the following questions: 

* Does the vox maps “explain the internet”? If not, what do they do? What do they show?
* How is the concept of territory different in both examples? Or in between geographical representations and traceroutes?
* What kind of territory are we talking about when we think about data flows?


## Tools
### Carto
https://carto.com/  
* Mapping and Geolocating platform

1. Log into Carto.com
2. Click on “new dataset” to upload your data (you can drag and drop your file or click browse)
3. Click Connect Dataset (Carto will automatically detect strong geolocation data, e.g. latitude and longitude coordinates, geographical polygons, etc) .

You will see your dataset with 2 added columns:

**cartodb_Id**: the Carto index for your data rows  
**the_geom**: this column stores the main geographical data types that Carto will map (if this column is empty, your geolocation data is missing from your dataset, or has mistakes on it)

4. Click Visualize. Carto will ask you to publish the map (click ok)
5. Once your map is public, you can still see and modify the “Data view” and “Map view”

IN THE DATA VIEW you can:
* Attempt to geolocate by string data (e.g. names of cities): click on the “geo” button (located next to “the_geom” column title). A window with different options to geolocate your data will open.
* Add more layers from different datasets, by pressing the blue plus symbol at the top of the right sidebar.

IN THE MAP VIEW you can:
* Modify the ways to visualize your data layers (use the wizard button on the right sidebar). Depending on your data types you may use quantities (density, simple , cluster, choropleth, heatmap, bubble), categories (category), time series (intensity, torque), or a combination (torque cat)
* Modify your base map and interface by clicking the “change basemap” and “options” at the bottom left corner of your map.


**Data for the example:** [listings_example.csv](listings_example.csv)  
**AIRBNB DATA**
http://insideairbnb.com/get-the-data.html  


### Activity
* Use data from another city from the Airbnb dataset to plot  information in Carto (*20 wikipoints*)

*OR*

* Try to plot in Carto some of the data that you’ve already collected in other sessions that may be used geographically (*9000 wikipoints*)


**Paste a link to your map in**: https://pad.riseup.net/p/DS19carto (remember to put your name next to the link)

## Required Readings
* Malik, Momin M. “Identifying Platform Effects in Social Media Data,” n.d., 9. (9 p.) [ignore equations!]
* Lury, Celia, Rachel Fensham, Alexandra Heller-Nicholas, Sybille Lammes, Angela Last, Mike Michael, and Emma Uprichard, eds. Routledge Handbook of Interdisciplinary Research Methods. 1st ed. Routledge, 2018. (Section 2, Chapter 12: visualizing data, 10 p.)

## Recommended readings
### VISUALISATION (II)
* Drucker, Johanna. Graphesis: Visual Forms of Knowledge Production. Cambridge, Massachusetts: Harvard University Press, 2014.
* Halpern, Orit. Beautiful Data: A History of Vision and Reason since 1945. Durham: Duke University Press Books, 2015.
* Peck, Evan M., Sofia E. Ayuso, and Omar El-Etr. “Data Is Personal: Attitudes and Perceptions of Data Visualization in Rural Pennsylvania.” ArXiv:1901.01920 [Cs], January 7, 2019. http://arxiv.org/abs/1901.01920.

### MAPS/TERRITORY
* Dodge, Martin, Rob Kitchin, and Chris Perkins. 2011. ‘Conceptualising Mapping’. In The Map Reader: Theories of Mapping Practice and Cartographic Representation, 1–7. Chichester, UK: John Wiley & Sons, Ltd.
* Eichenser, Kristen. “Data Extraterritoriality.” Texas Law Review, November 10, 2017. https://texaslawreview.org/data-extraterritoriality/.
* Bratton, Benjamin H. 2016. The Stack: On Software and Sovereignty. 1 edition. Cambridge, Massachusetts: The MIT Press.

## Other resources
* Periodic table of visualisation methods: http://www.visual-literacy.org/periodic_table/periodic_table.html   
**This table is really useful!**
* PODCAST: [Data is Personal with Evan Peck](https://datastori.es/data-is-personal-with-evan-peck/) (Data Stories #142)