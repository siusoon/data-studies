# EXAM DS19

[Pre-requisites for the exam](https://gitlab.com/xpablov/data-studies/blob/master/Exam.md#pre-requisites-for-the-exam)  
[Content](https://gitlab.com/xpablov/data-studies/blob/master/Exam.md#content)  
[Examples](https://gitlab.com/xpablov/data-studies/blob/master/Exam.md#examples)  
[FAQ](https://gitlab.com/xpablov/data-studies/blob/master/Exam.md#faq)  
[Bibliography per topic](https://gitlab.com/xpablov/data-studies/blob/master/Exam.md#recommended-bibliography-per-topic)

## Pre-requisites for the exam
Participation:
* To have 3 wiki-thoughts submitted on time OR 4 wiki-thoughts if submitted late
* To have submitted all 5 group mini-projects and presented at least once (**all groups have covered this requisite**)
* To have successfully presented your final group project (**all groups have covered this requisite**)

## Content

* 2-4 pages of the submission should provide a summary of the final project
* 8-10 pages are for a topic of your choice
* The whole exam should be 10-12 pages, but you may include an appendix in a separate file (for data sources, secondary images, etc). There is no limit for the appendix length, but consider that the censors will probably not look at an extremely large document
* I  recommend to add your final project submission (the one you uploaded to the wiki), as an appendix (for the censors).
* The exam must be written in English
* The exam is an individual submission
* The exam would be assessed by me (Pablo) and two other censors

### I. Your exam should comply with the basics of an academic essay:
1. Introduction
2. Research question(s) or presentation of the problem (*What?*, *Why?* and/or *Who?*)
3. Methods and Methodological aspects (*How?* *Why the how?*)
4. Discussion (results/findings and/or reflection) (*What not?*)
5. Conclusions or final comments
6. Bibliography

### II. The topic of your exam is open, but it must be related to the *learning outcomes* and *one or more topics* from the course. *Ideally*, you should also consider the *tools and/or methods* seen in the course

The data collection and use of tools is optional for the final exam, and it depends mostly on what you choose to write about. The primary part is the use of theories, reflection, and discussion of the class topics. *Ideally*, the exam could have both (tools and use of data + discussion), but the main concern is on the theory/discussion. 

#### II.1 Learning outcomes from the course:
* **Knowledge**:
    * Demonstrate an understanding of the role of data in society
    * Critically reflect on the use of data to conclude general conditions in the world and in digital environments
* **Skills**:
    * Use digital tools to collect, analyse and present data
    * Critically reflect on the production and use of data in specific cases
* **Competences**:
    * Critically analyse and consider the role of data in society, as well as the use and design of digital technologies for data collection and production

#### II.2 Topics from the course:

<img src="ds19_map.png" alt="drawing" width="600">  


*(click to enlarge)*

**ETHICS**

* ethical concerns (GDPR, data processing, sensitive and personal data, anonymous and identifiable data)

**"DATA"**

* data categories/taxonomies 
* 'raw' vs 'cooked' data (data vs fact)
* data capture (scraping), processing, and cleaning
* information and data (cybernetics)

**VISUALIZATION**

* *small* and *affective* data visualization (dear data)
* data visualization for manipulation (*data is personal*)
* data visualization as information graphics (rational metrics)
* power represented in visualizations
* data about places (geo) and data between places (flows)

**PLATFORMS**

* platformization
* politics of platforms
* cultural analytics
* text/document analysis

**METHODS AND NETWORKS**

* digital methods (redistribution, reassembling, digital "native" methods and objects)
* networks (sociometrics, visual analysis, )
* network flat ontology (ANT)
* controversy and actors detection and analysis (issue mapping)

**BIG DATA AND SURVEILLANCE**

* politics of big data
* datafication and dataism
* surveillance (disciplinary society, datavelliance, panoptic surveillance, liquid surveillance, digital surveillance, social surveillance)
* predictive policing systems
* surveillance/advertisement as business model

**CONTROL AND OPENNESS**

* grammars of action
* control as management of information (control society, cybernetics)
* tactical visibility and obfuscation
* open data (open software, open access, open knowledge)

### II.3. Tools and Methods from the course:
**TOOLS**

* Markdown
* Data Miner
* Raw
* Openrefine
* Voyant
* Instagrab
* Carto
* TCAT
* Gephi
* \* Pandas / Python / Jupyter-notebooks

**METHODS**

* Digital Methods ('native' digital objects) (Rogers)
* Document analysis
* Controversy analysis / issue mapping (Marres)
* Network analysis (Venturini)
* ANT: actors and community detection (Latour)
* Cultural analytics (Manovich)

## Examples:
*These examples are just for guiding purposes. Don't feel like you have to emulate them. Creativity is very well received.* 

A. **Example of a free assignment:** 

I am interested in the concept of datafication (i.e. turning social activity into data). I want to examine how subject data is categorized briefly in 3 different settings: [Facebook data points for ads](https://www.washingtonpost.com/news/the-intersect/wp/2016/08/19/98-personal-data-points-that-facebook-uses-to-target-ads-to-you/), [Danish official statistics on citizenship](https://www.dst.dk/en), and my social circle understanding of the same categories. This provides an overview of some categories, and not an in-depth analysis. My framework for this is based on an STS approach (technology and society interactions), and I will specifically use the concept of "grammar" as an iterative process of design that constrains or allows certain social activities. In this case, I will look for changes on the categories, and question if they open or restrain the possibilities to categorize a subject, from the point of view of the state, the company, and the citizen. My hypothesis is that Tim Berners Lee contract for a new internet is flawed, because it assumes that these 3 entities share an understanding of the same datafied subject, while in fact very different versions of a data subject exist. 

B. **Example using your final project:** 

I want to identify unexplored controversies from the TCAT database, based on my final group research project. My research group looked at X community, which apparently pushes to reformulate the discourse from climate *change* to climate *crisis*. While we couldn't find a group of skeptics directly rejecting X's group goals, I'm interested to see if there is an antagonism relation in the micro-politics of X group and other similar groups. I will first create a network of groups, that I see are in some sort of dialogue. Once these groups are identified (by username, URL, or exclusive hashtag), I will take a closer look at the interactions, to find the fine-grained discussion in between this groups: what differentiates them, and in what points they agree and disagree. My framework of analysis will then be controversy analysis / issue mapping, as I will try to locate points of contention that bring together, through a platform, previously disassembled communities. I  will question the role of the platforms for the creation of the communities, and which kind of political relationships they allow or deny. My methods for this work will be visual network analysis (gephi), co-occurring URL networks (issuecrawler), and document analysis (websites content). 

C. **Example using a mini-project:** 

I want to test the openness of data and metadata provided in certain websites. For this, I will use data from the mini-project 2 (collected through Data Miner scrapping, and visualised in Voyant). I  will analyse the tracker behaviour of the website, and the openness of the data: are the trackers explicit, and communicated to the user? Do the data formats follow FAIR and/or open standards? Is the Data accessible? I will argue that there is a surveillance practice in action, because the websites are using specific tracking practices, while the user is not provided with accessible data: that is, there is an uneven relationship between provider and user. For my framework, I will discuss the concepts of discipline and control, and in particular the concept of digital and liquid surveillance; and show how these concepts operate within the website's examples provided in the form of tracking, obfuscation, and lack of accessibility. 


**You may also find inspiration for your exams in these places:**

* https://tacticaltech.org
* https://labs.rs
* https://digitalmethods.net
* http://issuemapping.net/
* https://medialab.sciencespo.fr/en
* https://citizenlab.ca/
* http://publicdatalab.org/

## FAQ

***updated if/when questions arise***

<img src="teaching-politeness.png" width="500">


🙋  

**Q:**  *Where i should fit in my  2-4 pages of summary of the final project?*

**A:** The summary can be written as a separate part (as in: 1. summary of group project, 2. Exam [with introduction, research question, etc]), OR integrated into the sections (most likely in "Methods and Methodological aspects"). This would depend on how much the group project relates with the individual research. 

🙋  

**Q:** *Am I allowed to dig into a text from the curriculum and let that be the “main” text I analyse in my individual part of the exam assignment? Or do I have to find a text myself outside the curriculum?*

**A:** Yes, you can use a text from the syllabus as a core text to discuss. 

🙋  

**Q:**  *Do we HAVE to use the tools from class for our final exam, or if it's alright if we have other means for visualizing data?*

**A:** Yes, you can use other tools, as long as you still use the topics of the course

🙋  

**Q:**  *It is very difficult to break down the content of our already submitted report to 3 pages,would it be possible to focus on the most interesting results and after a  short description of the other tools used etc. to refer to our report?*

**A:** Yes, you can focus on the most relevant parts, but a sort of summary (even if incomplete) is required. I know your projects, but the censors won't, and the summary would give them a general idea of the project. I would recommend to include your final report as an appendix. 

🙋   

**Q:**  *Can I send you an idea for my exam, to see if it's adequate?*

**A:** Yes, I will do my best to answer on time.



## Recommended bibliography per topic

See [Recommended bibliography per topic](ext_bibliography.md) page

