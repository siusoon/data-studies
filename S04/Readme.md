# Session 4
Block 2: Platforms, Maps & Beautiful Data 

**SLIDES**: ([live](http://pablov.me/pres/ds19s04.html) / pdf)

## Downloads & installations prior to session:
- **Chrome**
- **Python 3.7** (Anaconda distribution) https://www.anaconda.com/distribution/ python 3.7 (if you have problems of space in your computer, you can install a basic version of python https://www.python.org/downloads/ , but this is NOT RECOMMENDED, since we will use Anaconda in later stages of the course)
- Download **Instagrab** (https://github.com/pwikstrom/instagrab) and unzip the folder in a easy-to-access location  (e.g. windows users, can put it in their Desktop)
- Download the **chrome driver** for your version from:  http://chromedriver.chromium.org/downloads (check your Chrome version in chrome://settings/help). Unzip this file to the “gecko” folder inside instagrab (if needed replace the file already there).

## Plan for the day:
* Platforms
* Dear data
* Tool: instagrab
* Mini-projects set-up

## Data contexts: platforms

**Manovich’s cultural analytics + instagram (Phototrails project)** 
* Selfie city (http://selfiecity.net/)
	* gender, age, smiles (all estimated through Mturk)
* Instagram city (http://phototrails.info/instagram-cities/) (imageplot, mturk)
	* software studies lab tools  qtip + imageplot (http://lab.softwarestudies.com/p/software-for-digital-humanities.html) 

**Gillespie (2010)**
Platform:

* computational (a supporting infrastructure)
* architectural (a raised surface)
* figurative (foundation for something)
* political (positioning, standing *on* an issue)

> “Allowing them to make a broadly progressive sales-pitch while also eliding tensions inherent in their service between user-generated and commercially-produced content, between cultivating community and serving up advertising, between intervening in the delivery of content and remaining neutral” (348)

**Helmond (2015)**
* Platformization of the web: dominant infrastructural and economic model
* Platforms as markets + modular technological architectures (Gawer 2014; Rieder & Sire 2014)
* “Infrastructures that can be programmed and built on” (Bogost & Montfort 2009)
* Web 2.0: web as a “robust development platform” (Tim O’Reilly 2004)
* “platform-ready” data: websites designed to be compatible to e.g. Facebook platform (thought javascript code as communication channel)

**Helmond et al (2019)**
* evolution as a platform
* platform “boundary-resources”can exercise power over institutional relationships

<img src="fb_marketing_ecosystem.png" width="500">

## Data contexts: dear data

**Visualizations**
* A narrative
* Directed to an audience
* Presenting particular information

**Some Examples of good informative *visuals***
* [Codebases: millions of lines of code](https://informationisbeautiful.net/visualizations/million-lines-of-code/)
* [Snake Oil superfoods](https://informationisbeautiful.net/visualizations/snake-oil-superfoods/)
* [Everynoise](http://everynoise.com/engenremap.html)
* [Hamilton](https://graphics.wsj.com/hamilton/) 

**Dear Data Project**

<img src="dd_phone_addiction1.jpg" width="500">
<img src="dd_phone_addiction2.jpg" width="500">
<img src="dd_beautiful_full.jpg" width="500">

*See also:*
* [Quantified selfie project](http://quantifiedselfie.us/)

#### Posavec-lupi recipe:

**Data** (it can be any data)  
+  
**Rules**: How to read it? (higher means *x*, lower means *y*)  
+  
**Visual variables**: shape, colour, size...

<img src="dd_recipe1.jpg" width="500">
<img src="dd_recipe2.jpg" width="500">
<img src="dd_recipe3.jpg" width="500">
<img src="dd_recipe4.jpg" width="500">

### Short activity: 
* Chose and analyse a beautiful data vis at https://informationisbeautiful.net/ (*100.5 wikipoints*)  
*OR*   
* Design a visualisation following Posavec-Lupi recipe (it can be about anything) (*1 million wikipoints*)

> While information may be infinite, the ways of structuring it are not. And once you have a place in which the information can be plugged, it becomes that much more useful. Your choice will be determined by the story you want to tell. Each way will permit a different understanding of the information—within each are many variations. However, recognizing that the main choices are limited makes the process less intimidating
(Wurman, Information Anxiety 2000)

## Tools
### Instagrab
https://github.com/pwikstrom/instagrab  
*You need a working version of python 3 (Anaconda is strongly recommended), and the chrome browser to run this tool*

* python script / web scraper
* does not use Instagram API

#### Instructions:

1. First: installing Anaconda Python distribution https://www.anaconda.com/distribution/ 
(if you have problems of space in your computer, you can install a basic version of python https://www.python.org/downloads/ , but this is NOT RECOMMENDED, since we will use Anaconda in later stages of the course)

2. Download instagrab (https://github.com/pwikstrom/instagrab) and put it into a simple location (e.g. windows users, can put it in their Desktop)
* Check your Chrome version in chrome://settings/help  
* Download the chrome driver for your version from:  http://chromedriver.chromium.org/downloads (choose your chrome version)
* unzip this file to the “gecko” folder inside instagrab.

3. Open a terminal and install Selenium

##### 3.A ON WINDOWS:  
 * start menu, type “anaconda prompt”

![img](https://lh3.googleusercontent.com/CmCUHexFHRYxZkqQbo7YSF8FhIbQ6rCboeE_vZt0iYGhkOHGARr56mLpGG2zx0HibQQnxiRBQP5gVffIAJxdiprSlmQifhu8VA0uxaYMBtqwOcL3mQvb-chnEvCyscuQQanBEzZM)   

 * a terminal window will open
 * install selenium driver by typing `pip install selenium`  

![img](https://lh4.googleusercontent.com/gLkpKCB1Unsr3AxDiAf9dUU1UQ5R9Wmtebex-uYHri1By3MDJli_lFy65bE9owHAi0WZV-GKTGbIqxuB2njFuM2inxkskoh3WnXTcA4fntTLs50GeS2KxBXhCg4X-CLyQvpRCBvk)

 * navigate to your instagrab folder by typing `cd \YOUR\LOCATION\FOR\INSTAGRAB`  
*OR* 
 * change the directory pathing by right-clicking on anaconda terminal > more > open fileplacering > right click > properties > Start i ...

![img](https://lh3.googleusercontent.com/MlAWBdyCXS-b8huAf1PfP4g5Dd3w-hUwNt43OsTf5p_05q2RGnooF7x0t-kjOcOnMj1OY8iyoRuRXbuuVO8PIY3qJp0nY-f5H9lh4zEp5atiE_6KqcAeyZ5ApXbKn30oa7n5da3M) 

##### 3.B ON MAC: 
 * open a terminal
 * install selenium driver by typing `pip install selenium`
 * navigate to your instagrab folder
 * copy path in finder: right click in folder > hold option button > copy “folder” as pathname
 * in the terminal type: cd [paste path]

4. Start instagrab example

* run `python ig_run.py example.txt` 

![img](https://lh5.googleusercontent.com/_08virtcVvXyJGytoGB5eNJYcIuHo8r_XpkS02KiULa8TuzFru9zkmunvZUfLYKIw1iqhZPq6A2wV3eCmxXZ3R1rJHVkGYwu7hxSBn-bzyo5E3R-3-1ywL009vfntuw5gWI69IXk)

**If you have problems, try:**
* Rename your example.txt to example_OLD.txt(inside your instagrab config folder)
* Download [example.txt](example.txt) and place it in the instagrab config folder
* Try to run the script again

### Activity
5. Change your query
* Change the configuration file to run your own queries. For doing this, create a new file and copypaste the content from the example (DO NOT modify the example version).  

* Change the path name to create a new project (data_path)

* In the data collection section, you can change the tags to scrape (scrape_tags) (it can be more than one); and the amount of items (scrape_count)

**Extended installation and use guide** [here](https://github.com/pwikstrom/instagrab/blob/master/docs/Get%20started%20with%20Instagrab.pdf)

## Mini project B2: Creative rationale
The goal for this project is less  focused on the methodology, and more on defining the rational basis for your mini-project, and on presenting it creatively.

For the second mini-project you should use at least one of the tools from this block (you can also use tools from previous blocks): *Voyant* and/or *Instagrab*. Try to use (if applicable) the topics of this and previous blocks (for this mini-project the questions on ethics are particularly relevant).
The main question should be addressed by your second mini-project:

***Why?***

Why did you choose to research your topic? What makes the topic relevant? Has no one asked this? Is your approach focused on social, technical, political, etc elements?

This is an explorative mini-project, so don't worry about finding the *right* answers or using the most *accurate* method. However, pay special attention to justifying your approach, and feel free to present it in a creative way.

As before, the mini-project **will be presented next week during class. Groups presenting will be randomly chosen on Tuesday before noon.** Some groups will also be chosen as replyers to comment on the work of your peers. However, **all groups should populate their wiki-projects.**



## For the next session:

**Sign up for a Carto student account using Github student pack**
https://carto.com/help/getting-started/student-accounts/#github-student-dev-pack---the-process
(you can also sign up for a free temporary non-student account, but this is not recommended)



## Required Readings

* Beer, David. The Data Gaze: Capitalism, Power and Perception. 1 edition. Thousand Oaks, CA: SAGE Publications Ltd, 2018. (Chapter 5: the diagnostic eye, 30 p.)
* Ruppert, Evelyn. “Category.” In Inventive Methods: The Happening of the Social, edited by Celia Lury and Nina Wakeford, 36–47. Routledge, 2012. (11 p.)

## Recommended readings
### PLATFORMS and APIs
* Bogost, Ian, and Nick Montfort. “Platform Studies: Frequently Questioned Answers,” December 12, 2009. https://escholarship.org/uc/item/01r0k9br.
* Gawer, Annabelle. “Bridging Differing Perspectives on Technological Platforms: Toward an Integrative Framework.” Research Policy 43, no. 7 (September 1, 2014): 1239–49. https://doi.org/10.1016/j.respol.2014.03.006.
* Gillespie, Tarleton. “The Politics of ‘Platforms.’” New Media & Society 12, no. 3 (May 2010): 347–64. https://doi.org/10.1177/1461444809342738.
* Helmond, Anne, David B. Nieborg, and Fernando N. van der Vlist. “Facebook’s Evolution: Development of a Platform-as-Infrastructure.” Internet Histories 3, no. 2 (April 3, 2019): 123–46. https://doi.org/10.1080/24701475.2019.1593667.
* Helmond, Anne. “The Platformization of the Web: Making Web Data Platform Ready.” Social Media + Society 1, no. 2 (July 1, 2015): 2056305115603080. https://doi.org/10.1177/2056305115603080.
* John, Nicholas A., and Asaf Nissenbaum. “An Agnotological Analysis of APIs: Or, Disconnectivity and the Ideological Limits of Our Knowledge of Social Media.” The Information Society 35, no. 1 (January 2019): 1–12. 
* Rieder, Bernhard, and Guillaume Sire. “Conflicts of Interest and Incentives to Bias: A Microeconomic Critique of Google’s Tangled Position on the Web.” New Media & Society 16, no. 2 (March 1, 2014): 195–211. https://doi.org/10.1177/1461444813481195.
* Zomorodi, Manoush. “Facing Our Weirdest Selves 2016.” Note to Self. Accessed September 26, 2018. https://www.wnycstudios.org/story/dear-data-quantified-self-tracking. (PODCAST)
### INSTAGRAM
* Hochman, Nadav. “Visualizing Instagram: Tracing Cultural Visual Rhythms.” In International AAAI Conference on Web and Social Media, 4, 2012. https://www.aaai.org/ocs/index.php/ICWSM/ICWSM12/paper/view/4782/5091. (4 p.)
* Hochman, Nadav, and Lev Manovich. “Zooming into an Instagram City: Reading the Local through Social Media.” First Monday 18, no. 7 (June 17, 2013). http://firstmonday.org/ojs/index.php/fm/article/view/4711. (~50 p.)
* Hu, Yuheng, Lydia Manikonda, and Subbarao Kambhampati. “What We Instagram: A First Analysis of Instagram Photo Content and User Types,” n.d., 4. (4p.)

### Visualisation (I)
* Posavec, Stefanie, Giorgia Lupi, and Maria Popova. Dear Data. New York: Princeton Architectural Press, 2016.
* Data is beautiful https://www.reddit.com/r/dataisbeautiful/
* Mona chalabi infographics https://www.instagram.com/monachalabi/


